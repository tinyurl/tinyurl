package com.diesel594.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class TinyUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    private String tinyUrlValue;

    @Column(unique = true, nullable = false)
    private String url;

    @Column(nullable = false)
    private long accessCount;

    public static final int TINY_URL_LENGTH = 6;

    public TinyUrl() {

    }

    public TinyUrl(String url, String tinyUrlValue) {
        this.url = url;
        this.tinyUrlValue = tinyUrlValue;
    }

    public String getTinyUrlValue() {
        return tinyUrlValue;
    }

    public void setTinyUrlValue(String tinyUrlValue) {
        this.tinyUrlValue = tinyUrlValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getAccessCount() {
        return accessCount;
    }

    public void setAccessCount(long accessCount) {
        this.accessCount = accessCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        TinyUrl tinyUrl = (TinyUrl) obj;
        if (!tinyUrlValue.equals(((TinyUrl) obj).tinyUrlValue)) return false;
        if (!url.equals(((TinyUrl) obj).url)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tinyUrlValue, url);
    }

    @Override
    public String toString() {
        return "TinyUrl [url=" + url + ", tinyUrlValue=" + tinyUrlValue + ", accessCount=" + accessCount + "]";
    }

    public long getId() {
        return id;
    }

    public void incrementAccessCount() {
        setAccessCount(accessCount + 1);
    }
}