package com.diesel594.dao;

import com.diesel594.model.TinyUrl;

import java.util.List;

public interface TinyUrlDao {
    List<TinyUrl> findAllTinyUrl();

    TinyUrl findByUrl(String url);

    TinyUrl findByTinyUrlValue(String tinyUrlValue);

    void saveTinyUrl(TinyUrl tinyUrl);

    void updateTinyURl (TinyUrl tinyUrl);

    void deleteTinyUrlByTinyUrlValue(String tinyUrlValue);

    void deleteTinyUrlByUrl(String url);

    boolean isTinyUrlExist(TinyUrl tinyUrl);
}
