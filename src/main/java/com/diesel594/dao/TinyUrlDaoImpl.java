package com.diesel594.dao;

import com.diesel594.model.TinyUrl;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository("tinyUrlDao")
public class TinyUrlDaoImpl extends AbstractDao implements TinyUrlDao {

    private static final Logger logger = Logger.getLogger(TinyUrlDaoImpl.class);

    @Override
    public List<TinyUrl> findAllTinyUrl() {
        logger.debug("Fetching all tinyUrl records");
        try {
            Query query = getSession().createQuery("from TinyUrl");
            List resultList = query.getResultList();
            logger.debug("Got " + resultList.size() + " records");
            if (resultList.size() > 0)
                return (List<TinyUrl>) query.getResultList();
        } catch (Exception e) {
            logger.error("Caught an error while fetching all records", e);
        }

        return null;
    }

    @Override
    public TinyUrl findByUrl(String url) {
        logger.debug("Looking for a tiny url record with the url: " + url);
        Query query = getSession().createQuery("from TinyUrl where url = :url");
        query.setParameter("url", url);

        logger.debug("Getting result");
        Object result = ((org.hibernate.query.Query) query).uniqueResult();
        if (result != null)
            return (TinyUrl) result;

        return null;
    }

    @Override
    public TinyUrl findByTinyUrlValue(String tinyUrlValue) {
        logger.debug("Looking for TinyUrl where tinyUrlValue is " + tinyUrlValue);
        try {
            Query query = getSession().createQuery("from TinyUrl where tinyUrlValue = :tinyUrlValue");
            query.setParameter("tinyUrlValue", tinyUrlValue);
            Object result = ((org.hibernate.query.Query) query).uniqueResult();
            if (result != null)
                return (TinyUrl) result;

        } catch (Exception e) {
            logger.error("Catch an error in findByTinyUrlValue method (tinyUrlValue = " + tinyUrlValue + ")", e);
        }

        return null;
    }

    @Override
    public boolean isTinyUrlExist(TinyUrl tinyUrl) {
        logger.debug("Checking if given TinyUrl (" + tinyUrl.toString() + ")record exists");
        Query query = getSession().createQuery("select count (*) from TinyUrl where url = :url and tinyUrlValue = :tinyUrlValue");
        query.setParameter("url", tinyUrl.getUrl());
        query.setParameter("tinyUrlValue", tinyUrl.getTinyUrlValue());
        Object result = ((org.hibernate.query.Query) query).uniqueResult();
        logger.debug("Found " + result + " record");
        return result != null;
    }

    @Override
    public void saveTinyUrl(TinyUrl tinyUrl) {
        logger.debug("Persisting TinyUrl (" + tinyUrl.toString() + ")");
        persist(tinyUrl);
    }

    @Override
    public void updateTinyURl(TinyUrl tinyUrl) {
        logger.debug("Updating TinyUrl (" + tinyUrl.toString() + ")");
        update(tinyUrl);
    }

    @Override
    public void deleteTinyUrlByTinyUrlValue(String tinyUrlValue) {
        logger.debug("Deleting TinyUrl with tinyUrlValue = " + tinyUrlValue);
        try {
            Query query = getSession().createSQLQuery("delete from TinyUrl where tinyUrlValue = :tinyUrlValue");
            query.setParameter("tinyUrlValue", tinyUrlValue);
            query.executeUpdate();
        } catch (Exception e) {
            logger.error("Caught an error while deleting TinyUrl record with tinyUrlValue = " + tinyUrlValue, e);
        }
    }

    @Override
    public void deleteTinyUrlByUrl(String url) {
        logger.debug("Deleting TinyUrl with url = " + url);
        try {
            Query query = getSession().createSQLQuery("delete from TinyUrl where url = :url");
            query.setParameter("url", url);
            query.executeUpdate();
        } catch (Exception e) {
            logger.error("Caught an error while deleting TinyUrl record with url = " + url, e);
        }
    }
}
