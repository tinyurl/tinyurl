package com.diesel594.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDao {

    private static final Logger logger = Logger.getLogger(AbstractDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void persist(Object object) {
        try {
            getSession().persist(object);
        }catch (Exception e) {
            logger.error("Caught an error while persisting " + object.toString(), e);
        }
    }

    public void update(Object object) {
        try {
            getSession().merge(object);
        }catch (Exception e) {
            logger.error("Caught an error while merging " + object.toString(), e);
        }
    }

    public void delete(Object object) {
        try {
            getSession().delete(object);
        } catch (Exception e) {
            logger.error("Caught an error while merging " + object.toString(), e);
        }
    }
}
