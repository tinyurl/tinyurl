package com.diesel594.service;

import com.diesel594.model.TinyUrl;

import java.util.List;

public interface TinyUrlService {

    String getURLByTinyUrl(TinyUrl tinyUrl);

    String getUrlByTinyUrlValue(String tinyLinkValue);

    TinyUrl findTinyUrlByUrl(String url);

    TinyUrl findTinyUrlByTinyUrlValue(String tinyUrlValue);

    TinyUrl createTinyUrl(String url);

    void saveTinyUrl(TinyUrl tinyUrl);

    void updateTinyUrl(TinyUrl tinyUrl);

    void deleteTinyUrl(String url);

    void incrementAccessCount(TinyUrl tinyUrl);

    List<TinyUrl> findAllTinyUrls();

    boolean isTinyUrlValueExist(String tinyUrlValue);

    boolean isTinyUrlExist(TinyUrl tinyUrl);

    boolean isUrlExist(String url);

    String generateTinyUrl();
}
