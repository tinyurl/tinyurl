package com.diesel594.service;

import com.diesel594.dao.TinyUrlDao;
import com.diesel594.model.TinyUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

@Service("tinyUrlService")
@Transactional
public class TinyUrlServiceImpl implements TinyUrlService {
    private final static String chars[] = {
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
    };

    @Autowired
    private TinyUrlDao tinyUrlDao;

    @Override
    public String getURLByTinyUrl(TinyUrl tinyUrl) {
        incrementAccessCount(tinyUrl);
        return tinyUrl.getUrl();
    }

    @Override
    public String getUrlByTinyUrlValue(String tinyUrlValue) {
        TinyUrl tinyUrl = tinyUrlDao.findByTinyUrlValue(tinyUrlValue);
        if (tinyUrl.getTinyUrlValue().equals(tinyUrlValue)) {
            incrementAccessCount(tinyUrl);
            return tinyUrl.getUrl();
        }

        return null;
    }

    @Override
    public TinyUrl findTinyUrlByUrl(String url) {
        return tinyUrlDao.findByUrl(url);
    }

    @Override
    public TinyUrl findTinyUrlByTinyUrlValue(String tinyUrlValue) {
        return tinyUrlDao.findByTinyUrlValue(tinyUrlValue);
    }


    @Override
    public TinyUrl createTinyUrl(String url) {
        return new TinyUrl(url, generateTinyUrl());
    }

    @Override
    public void saveTinyUrl(TinyUrl tinyUrl) {
        tinyUrlDao.saveTinyUrl(tinyUrl);
    }

    @Override
    public void updateTinyUrl(TinyUrl tinyUrl) {
        tinyUrlDao.updateTinyURl(tinyUrl);
    }

    @Override
    public void deleteTinyUrl(String url) {
        tinyUrlDao.deleteTinyUrlByUrl(url);
    }

    @Override
    public void incrementAccessCount(TinyUrl tinyUrl) {
        tinyUrl.incrementAccessCount();
        updateTinyUrl(tinyUrl);
    }

    @Override
    public List<TinyUrl> findAllTinyUrls() {
        return tinyUrlDao.findAllTinyUrl();
    }

    @Override
    public boolean isTinyUrlValueExist(String tinyUrlValue) {
        return tinyUrlDao.findByTinyUrlValue(tinyUrlValue) != null;
    }

    @Override
    public boolean isTinyUrlExist(TinyUrl tinyUrl) {
        return tinyUrlDao.isTinyUrlExist(tinyUrl);
    }

    @Override
    public boolean isUrlExist(String url) {
        return findTinyUrlByUrl(url) != null;
    }

    public String generateTinyUrl() {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < TinyUrl.TINY_URL_LENGTH; i++) {
            result.append(chars[new Random().nextInt(chars.length)]);
        }

        return result.toString();
    }
}
