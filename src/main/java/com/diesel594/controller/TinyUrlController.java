package com.diesel594.controller;

import com.diesel594.dao.AbstractDao;
import com.diesel594.model.TinyUrl;
import com.diesel594.service.TinyUrlService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class TinyUrlController {

    private static final Logger logger = Logger.getLogger(TinyUrlController.class);

    @Autowired
    private TinyUrlService tinyUrlService;

    // Get Tiny URLs list
    @RequestMapping(value = "/tinyurl/", method = RequestMethod.GET)
    public ResponseEntity<List<TinyUrl>> getUrlsList(){
        logger.debug("Getting full TinyUrl records list");
        List<TinyUrl> tinyUrls = tinyUrlService.findAllTinyUrls();
        if (tinyUrls == null)
            return new ResponseEntity<List<TinyUrl>>(HttpStatus.OK);

        return new ResponseEntity<List<TinyUrl>>(tinyUrls, HttpStatus.OK);
    }

    // Create tiny url
    @RequestMapping(value = "/tinyurl/", method = RequestMethod.POST)
    public ResponseEntity<TinyUrl> createTinyUrl(@RequestBody String url){
        logger.debug("Creating tiny url");
        logger.debug("Looking for an existing tiny url record (url = " + url);
        if (tinyUrlService.isUrlExist(url)){
            logger.debug("The tiny url for " + url + " exists");
            TinyUrl tinyUrl = tinyUrlService.findTinyUrlByUrl(url);
            return new ResponseEntity<TinyUrl>(tinyUrl, HttpStatus.CONFLICT);
        }

        logger.debug("Creating new tiny url record");
        TinyUrl tinyUrl = tinyUrlService.createTinyUrl(url);
        tinyUrlService.saveTinyUrl(tinyUrl);

        return new ResponseEntity<>(tinyUrl, HttpStatus.CREATED);
    }

    //Find by url or tinyUrl and delete tiny url
    @RequestMapping(value = "/tinyurl/{value}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTinyUrl(@PathVariable String value){
        logger.debug("Looking for a record with url/tinyUrl " + value + " and deleting it");
        TinyUrl tinyUrl = tinyUrlService.findTinyUrlByUrl(value);
        if (tinyUrl == null)
            tinyUrl = tinyUrlService.findTinyUrlByTinyUrlValue(value);

        if (tinyUrl == null) {
            logger.debug("No tiny url records with " + value + " found");
            return new ResponseEntity<String>(value, HttpStatus.NOT_FOUND);
        }

        logger.debug("Deleting " + tinyUrl.toString());
        tinyUrlService.deleteTinyUrl(tinyUrl.getUrl());
        return new ResponseEntity<String>(value, HttpStatus.OK);
    }

    // Get full url using tiny url and redirect to it
    @RequestMapping(value = "/{tinyUrlValue}", method = RequestMethod.GET)
    public ResponseEntity<String> redirectToFullUrl(@PathVariable String tinyUrlValue) {
        logger.debug("Find and return full url with " + tinyUrlValue + " value");

        TinyUrl tinyUrl = tinyUrlService.findTinyUrlByTinyUrlValue(tinyUrlValue);
        if (tinyUrl == null) {
            logger.debug("tinyUrlValue " + tinyUrlValue + " not found");
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }

        tinyUrlService.incrementAccessCount(tinyUrl);

        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setLocation(new URI(tinyUrl.getUrl()));
            headers.setConnection("close");
            logger.debug("Redirecting to " + tinyUrl.getUrl());
            return new ResponseEntity<String>(headers, HttpStatus.MOVED_PERMANENTLY);
        } catch (Exception e){
            logger.debug("Caught an error while redirecting to " + tinyUrl.getUrl(), e);
            return new ResponseEntity<String>(tinyUrl.getUrl(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // Get full url using tiny url and redirect to it
    @RequestMapping(value = "/tinyurl/{tinyUrlValue}", method = RequestMethod.GET)
    public ResponseEntity<String> returnFullUrl(@PathVariable String tinyUrlValue) {
        logger.debug("Find and return full url with tinyUrlValue = " + tinyUrlValue + " value");

        TinyUrl tinyUrl = tinyUrlService.findTinyUrlByTinyUrlValue(tinyUrlValue);
        if (tinyUrl == null) {
            logger.debug("Such tiny url does not exist (tinyUrlValue = "+tinyUrlValue+")");
            return new ResponseEntity<String>(tinyUrlValue, HttpStatus.NOT_FOUND);
        }

        tinyUrlService.incrementAccessCount(tinyUrl);
        return new ResponseEntity<String>(tinyUrl.getUrl(), HttpStatus.OK);
    }

}
